#include <stdio.h>
#include <fcntl.h>
#include <sys/mman.h>
#include <MiniFB.h>

#define WINDOW_WIDTH 1920
#define WINDOW_HEIGHT 1080
#define WINDOW_PIXELS (WINDOW_HEIGHT * WINDOW_WIDTH)



int main(int argc, char **argv) {
    int fd = open(argv[1], O_RDWR);
    void *mem = mmap(NULL, WINDOW_PIXELS*sizeof(unsigned int), PROT_READ, MAP_SHARED, fd, 0);

    struct mfb_window *window = mfb_open_ex("Test!", WINDOW_WIDTH, WINDOW_HEIGHT, 0);

    if (!window) {
        perror("Failed to create window");
        return -1;
    }

    mfb_set_target_fps(240);

    while (mfb_wait_sync(window)) {
        if (mfb_update(window, mem) < 0)
            break;
    }
}
